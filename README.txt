Hi, thanks for taking the time to look through this. I built all this using Visual Studio 2019, so all it reuires is building
and then running using IIS Express (Google Chrome). You should when running have it open localhost in a tab showing you the
Vending Machine drinks options, if you select a specific drink by its button it will take you to the corresponding recipe/action 
page.

Because I was a teensy bit forgetful, I wrote and planned everything in my notepad instead of into kanban, which would have
been much more legible for you. So as a consolation below is the details of what I did transcribed from my messy notes and 
also explains a bit of what you can see in the git repo:

-----------      Start      -----------      
- Created an MVC ASP.NET Web Application in Visual Studio
- Pushed to master in repository
-----------      Vending Machine Homepage      -----------     
- Created a new branch named landing-page
- On this new branch I removed all unnecessary pages that comes included in the default and created a simple page that 
  would contain the three drink options (i.e. be the interface of the vending machine)
- I added some placeholders of the options with "buttons" and made the starting page
- Once I ensured that this all worked fine I then merged this back into the master branch
-----------      Lemon Tea View      -----------  
- Created a new feature branch called lemon-tea
- Created a new view that displayed the lemon tea recipe
- Linked the placeholder button to link to the new view
- After checking it worked I then merged this branch back into master
-----------      Coffee View      -----------  
- Repeated the actions in Lemon Tea View but for coffee
-----------      Hot Chocolate View      -----------  
- Repeated the actions in Lemon Tea View but for hot chocolate
-----------      Models      -----------  
- Created a new feature branch called recipe-model
- On this branch I created a VendingMachineOptionModel object and a RecipeModel object
- I did this so I could make the views more dynamic and therefore scalable for any future recipes added, the models could
  then replace the hardcoded data in the views
- I first started with the VendingMachineOptionModel incorporating it into the vending machine homepage and instead placed
  the data hardcoded into the HomeController (temporarily)
- Checked this was able to populate correctly
- Then implemented the RecipeModel object which meant I could reduce the 3 seperate recipe views from above into one view
- After checking it worked I then merged this branch back into master
-----------      JSON      -----------  
- Created a new feature branch called json-parsing
- Moved the hard-coded data into a JSON file and edited the controller to read in the data from the JSON file and parse it
- Once I was sure this worked correctly I then merged it into master
-----------      Refactoring      ----------- 
- Created a new feature branch called refactor
- In this branch I've cleaned up the code - such as deleting some unnecessary code, renaming bits of code, creating a seperate 
  class to read the JSON file and moving this out of the controller etc.
- After checking this all works I then merged back into master
- I then also recloned the repo and checked it still worked
-----------      Finish      -----------


It's been a little while since I've done this from scratch and I was a bit limited for time, there is definitely things that 
I would improve on such as the way that the selected recipe is passed from the Vending Machine View to the controller and 
perhaps instead of using a JSON file for the drinks you could alter it to use a database, and of course some actual tests. 
Nevertheless it works and given the time I'm pretty pleased with how it's turned out.

Thanks for taking the time to look through all this! Have a great day!
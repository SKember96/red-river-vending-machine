﻿using System.Web.Mvc;
using VendingMachine.Models;

namespace VendingMachine.Controllers
{
    public class RecipeController : Controller
    {
        public ActionResult Recipe(RecipeModel recipeModel)
        {
            return View(recipeModel);
        }
    }
}
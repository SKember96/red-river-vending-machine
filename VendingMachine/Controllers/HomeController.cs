﻿using System.Web.Mvc;
using VendingMachine.Helper;
using VendingMachine.Models;

namespace VendingMachine.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult VendingMachine()
        {
            
            ViewBag.Message = "Your vending machine view.";
            string filePath = Server.MapPath("~/App_Data/machineoptions.json");
            VendingMachineOptionRetriever vendingMachineOptionRetriever = new VendingMachineOptionRetriever();
            VendingMachineOptionsModel vendingMachineOptionsModel = vendingMachineOptionRetriever.RetrieveVendingMachineOptions(filePath);

            return View(vendingMachineOptionsModel);
        }
    }
}
﻿using VendingMachine.Models;
using Newtonsoft.Json;
using System.IO;

namespace VendingMachine.Helper
{
    public class VendingMachineOptionRetriever
    {
        public VendingMachineOptionsModel RetrieveVendingMachineOptions(string filePath)
        {
            VendingMachineOptionsModel vendingMachineOptionsModel;
            using (StreamReader reader = new StreamReader(filePath))
            {
                string jsonMachineOptions = reader.ReadToEnd();
                vendingMachineOptionsModel = JsonConvert.DeserializeObject<VendingMachineOptionsModel>(jsonMachineOptions);
            }
            return vendingMachineOptionsModel;
        }
    }
}
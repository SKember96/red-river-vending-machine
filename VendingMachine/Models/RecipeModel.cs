﻿using System.Collections.Generic;

namespace VendingMachine.Models
{
    public class RecipeModel
    {
        public int RecipeId { get; set; }

        public string Name { get; set; }

        public List<string> Instructions { get; set; }
    }
}
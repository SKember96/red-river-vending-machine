﻿using System.Collections.Generic;

namespace VendingMachine.Models
{
    public class VendingMachineOptionsModel
    {
        public int VendingMachineId { get; set; }

        public List<RecipeModel> Options  { get; set; }
    }
}